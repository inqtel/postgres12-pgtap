
# postgres12-pgtap

**postgres12-pgtap** builds and stores a Docker image that contains [pg_prove](https://metacpan.org/pod/distribution/TAP-Parser-SourceHandler-pgTAP/bin/pg_prove), [pgTap](https://pgtap.org) and [PostgreSQL](https://www.postgresql.org). The image is automatically built and pushed to the [Container Registry](https://gitlab.com/inqtel/postgres12-pgtap/container_registry) with GitLab's CI/CD service.

Projects can use to this image to run their pgTap unit tests locally or from a Gitlab CI job.

#### Start a Postgres Instance:


> docker run --name postgres12-pgtap -e POSTGRES_PASSWORD= mysecretpassword -e POSTGRES_USER=superuser -e POSTGRES_DB=your_database inqtel/postgres12-pgtap


#### Run pgtap tests with pg_prove locally

> docker exec -it postgres12-pgtap /bin/bash -c "export PGPASSWORD= mysecretpassword && pg_prove -U superuser -h 0.0.0.0 -d your_database tests/test.sql"


#### Run pgtap tests via GitLab CI

See [.gitlab-ci.yml](https://gitlab.com/inqtel/postgres12-pgtap/-/blob/production/.gitlab-ci.yml)

